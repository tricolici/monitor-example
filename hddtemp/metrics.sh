#!/bin/sh

echo -n "Content-Type: text/plain\r\n"
echo -n "\r\n"

for f in /dev/sd?
do
  temp=$(sudo hddtemp -n -u C $f)
  echo "hdd_temperature{name=\"$f\"} $temp"
done
